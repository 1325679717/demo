package com.example.myt.easy_frame.presenter;

import android.util.Log;

import com.example.myokhttp.constants.Contants;
import com.example.myokhttp.listener.StringCallback;
import com.example.myokhttp.network.BaseReuestCall;
import com.example.myokhttp.network.GetParamsUtill;
import com.example.myt.easy_frame.IuserLoginView;
import com.example.myt.easy_frame.base.BasePresenter;
import com.example.myt.easy_frame.bean.Channel;
import com.example.myt.easy_frame.bean.User;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;


public class UserLoginPresenter extends BasePresenter<IuserLoginView> {

    BaseReuestCall requestCall = new BaseReuestCall();

    public UserLoginPresenter() {
        this.requestCall = new BaseReuestCall();
    }
    public void login(){
        if (isViewAttached()) {
            getView().showLoading();
        }

        GetParamsUtill getParamsUtill = new GetParamsUtill(Contants.strUrl);
        getParamsUtill.add("q","可爱");
        requestCall.get(getParamsUtill, new StringCallback<List<Channel>>() {


            @Override
            public void onResponse(int code, List<Channel> data) {
                Log.i("UserLoginPresenter","data =" + data.size());

            }

            @Override
            public void onFailure(Throwable e) {

                if (isViewAttached()) {
                    getView().showFailedError();
                    getView().hideLoading();
                }
            }
        });

    }
    public void clear(){

        if (isViewAttached()) {
            getView().clearUserName();
            getView().clearPassword();
        }
    }
}
