package com.example.myokhttp.network;

import android.net.Uri;

import com.example.myokhttp.callback.CallBackImpl;
import com.example.myokhttp.listener.StringCallback;
import com.example.myokhttp.parser.JsonParser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.FormBody;
import okhttp3.Request;

public class BaseReuestCall {

    public static Map<String,String> map = null;

    public static Map<String, String> getMap() {
        if (map == null){
            synchronized (BaseReuestCall.class) {
                if (map == null) {
                    map = new HashMap<>();
                }
            }
        }
        if (map.size() > 0)
            map.clear();
        return map;
    }

    public <T> void post(GetParamsUtill paramsUtill, final StringCallback<T> callback){
        Map<String,String> map = getMap();
        Request.Builder builder = paramsUtill.getParams(map);
        FormBody.Builder pBuilder = new FormBody.Builder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            pBuilder.add(entry.getKey(),entry.getValue());
        }

        builder.url(paramsUtill.getHttpUrl());
        builder.post(pBuilder.build());
        final HttpUtils httpUtils = HttpUtils.getInstance();
        httpUtils.getOkHttpClient().newCall(builder.build()).enqueue(new CallBackImpl<T>(new JsonParser<T>(){}){
            @Override
            public void onSuccess(int code, T t) {
                callback.onResponse(code,t);
            }

            @Override
            public void onFailure(Throwable e) {
                callback.onFailure(e);
            }
        });
    }


    public <T> void get(GetParamsUtill paramsUtill,final StringCallback<T> callback){

        Map<String,String> map = getMap();
        Request.Builder builder = paramsUtill.getParams(map);
        final HttpUtils httpUtils = HttpUtils.getInstance();

        System.out.println("appendParams = "+ appendParams(paramsUtill.getHttpUrl(),map));

        builder.url(appendParams(paramsUtill.getHttpUrl(),map));
        httpUtils.getOkHttpClient().newCall(builder.build()).enqueue(new CallBackImpl<T>(new JsonParser<T>(){}) {
            @Override
            public void onSuccess(int code, T t) {
                callback.onResponse(code,t);
            }

            @Override
            public void onFailure(Throwable e) {
                System.out.println("e = "+ e.toString());
                callback.onFailure(e);
            }
        });
    }
    protected String appendParams(String url, Map<String, String> params)
    {
        if (url == null || params == null || params.isEmpty())
        {
            return url;
        }
        Uri.Builder builder = Uri.parse(url).buildUpon();
        Set<String> keys = params.keySet();
        Iterator<String> iterator = keys.iterator();
        while (iterator.hasNext())
        {
            String key = iterator.next();
            builder.appendQueryParameter(key, params.get(key));
        }
        return builder.build().toString();
    }
}
