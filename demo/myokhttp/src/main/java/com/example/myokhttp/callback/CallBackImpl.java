package com.example.myokhttp.callback;

import com.example.myokhttp.network.HttpUtils;
import com.example.myokhttp.parser.OkBaseParser;
import com.example.myokhttp.utils.Platform;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public abstract class CallBackImpl<T> implements Callback {

    final Platform platform = HttpUtils.getInstance().getmPlatform();
    private OkBaseParser<T> mParser;
    public CallBackImpl(OkBaseParser<T> mParser){
        this.mParser = mParser;
    }
    @Override
    public void onFailure(Call call, final IOException e) {
        platform.execute(new Runnable() {
            @Override
            public void run() {

                onFailure(e);
            }
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        final int code = mParser.getCode();
        final T t = mParser.parseResponse(response);
        platform.execute(new Runnable() {
            @Override
            public void run() {
                onSuccess(code,t);
            }
        });
    }

    public abstract void onSuccess(int code, T t);

    public abstract void onFailure(Throwable e);
}
