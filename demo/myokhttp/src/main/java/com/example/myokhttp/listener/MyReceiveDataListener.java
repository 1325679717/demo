package com.example.myokhttp.listener;


public interface MyReceiveDataListener {
    void onReceive(int action, String code, String msg, Object data);

    void onFile(int action, Object data);
}
