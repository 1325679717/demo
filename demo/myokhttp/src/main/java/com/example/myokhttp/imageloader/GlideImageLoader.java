package com.example.myokhttp.imageloader;

import android.app.Activity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.myokhttp.R;
import com.example.myokhttp.listener.ImageLoader;

/**
 * Created by Administrator on 2017/7/28.
 */

public class GlideImageLoader implements ImageLoader {
    @Override
    public void displayImage(Activity activity, String path, ImageView imageView, int width, int height) {

        Glide
              .with(activity)
              .load(path)
//                .placeholder(R.drawable.loading_spinner)
              .into(imageView);
    }

    @Override
    public void clearMemoryCache() {

    }
}
