package com.example.myokhttp.network;


import com.example.myokhttp.utils.Platform;

import okhttp3.OkHttpClient;


public class HttpUtils {
    public static final long DEFAULT_MILLISECONDS = 10_000L;
    private volatile static HttpUtils mInstance;
    private OkHttpClient mOkHttpClient;
    private Platform mPlatform;

    public HttpUtils(OkHttpClient okHttpClient)
    {
        if (okHttpClient == null)
        {
            mOkHttpClient = new OkHttpClient();
        } else
        {
            mOkHttpClient = okHttpClient;
        }

        mPlatform = Platform.get();
    }

    public Platform getmPlatform(){
        return mPlatform;
    }
    public OkHttpClient getOkHttpClient()
    {
        return mOkHttpClient;
    }

    public static HttpUtils initClient(OkHttpClient okHttpClient)
    {
        if (mInstance == null)
        {
            synchronized (HttpUtils.class)
            {
                if (mInstance == null)
                {
                    mInstance = new HttpUtils(okHttpClient);
                }
            }
        }
        return mInstance;
    }

    public static HttpUtils getInstance()
    {
        return initClient(null);
    }

}
