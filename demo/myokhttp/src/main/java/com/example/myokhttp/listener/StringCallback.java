package com.example.myokhttp.listener;


public interface StringCallback<T> {
    void onResponse(int code, T t);

    void onFailure(Throwable e);
}
