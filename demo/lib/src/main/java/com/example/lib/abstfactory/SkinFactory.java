package com.example.lib.abstfactory;

/**
 * Created by Administrator on 2017/7/31.
 */

public interface SkinFactory {
    public Button createButton();

    public TextField createTextField();

}
