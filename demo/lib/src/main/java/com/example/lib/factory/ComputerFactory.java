package com.example.lib.factory;

/**
 * Created by Administrator on 2017/7/27.
 */

public class ComputerFactory {
    public <T extends Computer> T production(Class<T> tClass){
        T tClass1 = null;
        try {
            tClass1 = tClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return tClass1;
    }
}
