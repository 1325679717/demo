package com.example.lib.strategy;

public class myClass {
    public static void main(String[] args) {
        MemberInterface memberInterface = new OrdiMemberImpl();
//        MemberInterface memberInterface = new VipMemberImpl();
        Movie movie = new Movie(memberInterface);
        movie.watch();
    }
}
