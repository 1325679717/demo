package com.example.lib.deputize;

import com.example.lib.strategy.MemberInterface;
import com.example.lib.strategy.Movie;
import com.example.lib.strategy.OrdiMemberImpl;

import java.lang.reflect.Proxy;

public class myClass {
    public static void main(String[] args) {
        BankInterface concreteBank = new ConcreteBank();
        DynamicPorxy dynamicPorxy = new DynamicPorxy(concreteBank);
        BankInterface ATM = (BankInterface) Proxy.newProxyInstance(concreteBank.getClass().getClassLoader(),new Class[]{BankInterface.class},dynamicPorxy);
        ATM.save();

        ATM.take();
    }
}
