package com.example.lib.factory;

public class myClass {
    public static void main(String[] args) {
        ComputerFactory computerFactory = new ComputerFactory();
        Computer computer = computerFactory.production(Alienware.class);
        computer.intr();
    }
}
