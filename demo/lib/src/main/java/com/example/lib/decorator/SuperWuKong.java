package com.example.lib.decorator;

/**齐天大圣类
 * Created by Administrator on 2017/7/27.
 */

public class SuperWuKong implements Sourceable {
    private Sourceable sourceable;

    public SuperWuKong(Sourceable sourceable) {
        this.sourceable = sourceable;
    }

    @Override
    public void method() {

        System.out.print("拥有凤翅紫金冠、锁子黄金甲、如意金箍棒的");
        sourceable.method();
    }
}
