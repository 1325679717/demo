package com.example.lib.template;

/**
 * Created by Administrator on 2017/7/26.
 */

public class RelativeLayout extends ViewGroup {


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        System.out.println("我是RelativeLayout onMeasure()");
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        System.out.println("我是RelativeLayout onLayout()");
    }
}
