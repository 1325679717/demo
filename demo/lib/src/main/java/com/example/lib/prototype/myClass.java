package com.example.lib.prototype;

public class myClass {
    public static void main(String[] args) {
        WordDocument wordDocument = new WordDocument();

        wordDocument.setText("这是一篇文档");
        wordDocument.addImage("图片1");
        wordDocument.addImage("图片2");
        wordDocument.addImage("图片3");
        wordDocument.showDocument();

        WordDocument doc2 = wordDocument.clone();
        doc2.showDocument();

        doc2.setText("这是修改过的Doc2文本");
        doc2.showDocument();

        wordDocument.showDocument();
    }
}
