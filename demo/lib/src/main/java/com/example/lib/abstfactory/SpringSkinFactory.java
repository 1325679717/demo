package com.example.lib.abstfactory;

/**
 * Created by Administrator on 2017/7/31.
 */

public class SpringSkinFactory implements SkinFactory {
    @Override
    public Button createButton() {
        return new SpringButton();
    }

    @Override
    public TextField createTextField() {
        return new SpringTextField();
    }
}
