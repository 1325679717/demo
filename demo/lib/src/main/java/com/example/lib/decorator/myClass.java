package com.example.lib.decorator;

public class myClass {
    public static void main(String[] args) {
        System.out.println("装备之前");
        Sourceable wukong = new WuKong();
        wukong.method();
        System.out.println("装备之后");
        Sourceable sourceable = new SuperWuKong(wukong);
        sourceable.method();
    }
}
