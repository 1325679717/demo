package com.example.lib.abstfactory;

public class myClass {
    public static void main(String[] args) {
        SkinFactory factory = new SpringSkinFactory();
        Button button = factory.createButton();
        TextField textField = factory.createTextField();

        button.display();
        textField.display();
    }
}
