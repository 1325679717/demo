package com.example.lib.adapter;

/**
 * Created by Administrator on 2017/8/1.
 */

public interface ScoreOperation {
    public int[] sort(int array[]); //成绩排序
    public int search(int array[],int key); //成绩查找
}
