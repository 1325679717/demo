package com.example.lib.template;

/**
 * Created by Administrator on 2017/7/26.
 */

public abstract class ViewGroup {

    protected void onMeasure(int widthMeasureSpec,int heightMeasureSpec){

    }
    public final void measure(int widthMeasureSpec,int heightMeasureSpec){
//        int widthMeasureSpec = 0;
//        int heightMeasureSpec = 0;
        System.out.print("ViewGroup 测量的规格逻辑");
        onMeasure(widthMeasureSpec,heightMeasureSpec);
    }
    protected abstract void onLayout(boolean changed,int l,int t,int r,int b);

    public final void layout(int l,int t,int r,int b){
        boolean changed = true;

        System.out.println("layout 自己的一些逻辑");
        onLayout(changed,l,t,r,b);
    }
}
