package com.example.lib.strategy;

/**
 * Created by Administrator on 2017/7/26.
 */

public class Movie {
    private MemberInterface member;
    public Movie(MemberInterface member){
        this.member = member;
    }
    public void watch(){
        member.mType();
    }
}
