package com.example.lib.deputize;

/**
 * Created by Administrator on 2017/7/26.
 */

public class ConcreteBank implements BankInterface {
    @Override
    public void take() {
        System.out.println("take money");
    }

    @Override
    public void save() {
        System.out.println("save money");
    }
    public void activate(){
        System.out.println("activate a bank card");
    }
}
