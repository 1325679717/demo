package com.example.lib.template;

/**
 * Created by Administrator on 2017/7/26.
 */

public class ViewRootImpl {
    ViewGroup viewGroup = null;
    public ViewRootImpl(ViewGroup viewGroup){
        this.viewGroup = viewGroup;
    }
    public void performTraversals(){
        viewGroup.measure(0,0);
        viewGroup.layout(0,0,0,0);
    }
}
